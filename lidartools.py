import serial
import rplidar
import matplotlib.pyplot as plt
import numpy as np
import chalk

def radial_to_cartesian(radialPoint):
    # pointArray in the form [quality angle distance]
        #print(radialPoint[2] * np.exp(1j * radialPoint[1]))
        cartesianComplex = radialPoint[2] * np.exp(1j * radialPoint[1])
        cartesianPoint = [radialPoint[0], cartesianComplex.real, cartesianComplex.imag]
        return(cartesianPoint)
def radial_to_cartesian_array(radialArray):
    cartesianArray = np.copy(radialArray)
    for n,radialPoint in enumerate(radialArray):
        cartesianPoint = radial_to_cartesian(radialPoint)
        cartesianArray[n] = cartesianPoint
    return cartesianArray


def main2():
    print(chalk.green('Main Routine Started'))

    serialPort = "/dev/ttyUSB0"
    print(chalk.red('Connecting to rplidar at : ' + serialPort))
    lidar = rplidar.RPLidar(serialPort)
    input(chalk.blue("Press any key to start iterating over lidar scans"))
    iterator = lidar.iter_scans()
    try:
        while True:
            new_scan = next(iterator)
            print(new_scan)
            input(chalk.green("enter to continue, ") +chalk.cyan('ctrl + c to stop'))
    except KeyboardInterrupt:
        print('done')
    input(chalk.blue("Press enter to stop lidar"))
    lidar.stop()
    lidar.stop_motor()
    input(chalk.blue("Press enter to disconnect from lidar"))
    lidar.disconnect()
    print(chalk.red('lidar disconnected'))
    print(chalk.green('Main Routine Finished'))
def main():
    ps = np.array([[3, 45, 6],[3,60,8]],dtype=float)
    p = ps[0]
    print(radial_to_cartesian_array(ps))
    #print(radial_to_cartesian(p))
if __name__ == '__main__':
    main2()
