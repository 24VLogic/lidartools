
'THANK YOU SKROBO'
from rplidar import RPLidar
import matplotlib.pyplot as plt
import numpy as np
import matplotlib.animation as animation
from time import sleep
from sklearn.cluster import DBSCAN
from sklearn.preprocessing import StandardScaler


PORT_NAME = '/dev/ttyUSB0'
DMAX = 4000
IMIN = 0
IMAX = 50

def update_line(num, iterator, line,line2):
    scan = next(iterator)
    offsets = np.array([(np.radians(meas[1]), meas[2]) for meas in scan])
    line.set_offsets(offsets)
    intens = np.array([meas[0] for meas in scan])
    line.set_array(intens)

    xy = np.array([(meas[2]*np.cos(np.radians(meas[1])),meas[2]*np.sin(np.radians(meas[1]))) for meas in scan])
    line2.set_array(intens)
    line2.set_offsets(xy)
    return line, line2

def run():
    lidar = RPLidar(PORT_NAME)
    lidar.stop()
    lidar.stop_motor()
    lidar.clear_input()
    sleep(1)
    lidar.start_motor()
    fig = plt.figure()
    ax = plt.subplot(211,projection = 'polar')
    line = ax.scatter([0, 0], [0, 0], s=5, c=[IMIN, IMAX],
                           cmap=plt.cm.Greys_r, lw=0)
    ax.set_rmax(DMAX)
    ax.grid(True)
    ax2 = plt.subplot(212,aspect= 'equal')
    ax2.set_xlim(-DMAX/2,DMAX/2)
    ax2.set_ylim(-DMAX/2,DMAX/2)
    line2 = ax2.scatter([0, 0], [0, 0], s=5, c=[IMIN, IMAX],
                           cmap=plt.cm.Greys_r, lw=0)
    #fig2 = plt.figure()
    #ax2 = plt.subplot(111)
    #line2 = ax.plot([0],[0])
    iterator = lidar.iter_scans()
    ani = animation.FuncAnimation(fig, update_line,frames = 30,
        fargs=(iterator, line,line2), interval=60)
    ani.save('scans.gif', dpi=80, writer='imagemagick')
    #plt.show()
    lidar.stop()
    lidar.stop_motor()
    lidar.clear_input()
    lidar.disconnect()


if __name__ == '__main__':
    run()
